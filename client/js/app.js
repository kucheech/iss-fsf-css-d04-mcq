(function () {
    var app = angular.module("QuizApp", []);

    app.controller("QuizCtrl", ["$http", QuizCtrl]);

    function QuizCtrl($http) {
        var self = this; // viewmodel

        self.init = function () {
            $http.get("/questions").then(function (response) {
                var question = response.data;
                // console.log(question);
                self.id = question.id;
                self.question = question.text;
                self.options = question.options;
            }).catch(function (e) {
                console.log(e);
            });
        };

        self.init();

        self.submitAnswer = function() {
            // console.log(self.id);
            // console.log(self.choice);
            // console.log(self.comments);
            self.attempt = {
                id: self.id,
                choice: self.choice,
                comment: self.comment
            };
            $http.post("/answer", self.attempt)
            .then(function (result) {
                var isCorrect = result.data.isCorrect;
                // console.log(isCorrect);
                self.status = isCorrect ? "Correct" : "Wrong";
              
            }).catch(function (e) {
                console.log(e);
            });
        }

    }

})();