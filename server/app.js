const express = require('express');
const app = express();

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// // Respond with Hello World! on the homepage:
// app.get('/', function (req, res) {
//     res.send('Hello World!')
// })

var questions = [{
    id: 0,
    text: "What is your favourite colour?",
    options: ["red", "green", "blue", "yellow", "purple"],
    answer: 0
},
{
    id: 1,
    text: "What is your lucky colour?",
    options: ["red", "green", "blue", "yellow", "purple"],
    answer: 1
},
{
    id: 2,
    text: "What is your underwear colour?",
    options: ["red", "green", "blue", "yellow", "purple"],
    answer: 2
}
];


app.use(express.static(__dirname + "/../client/"));

// Respond to POST request on the root route (/), the application’s home page:
app.post('/', function (req, res) {
    res.send('Got a POST request')
});

app.get('/questions', function (req, res) {
    shuffle(questions);
    //make a copy of js variable as suggested at https://stackoverflow.com/a/29096222
    var question = JSON.parse(JSON.stringify(questions[0])); //to send only the first question
    delete question["answer"]; //without the answer
    res.json(question);
    // res.json(questions[0]);
});

app.post('/answer', function (req, res) {
    var isCorrect = checkAnswer(req.body.id, req.body.choice);
    var answer = { isCorrect: isCorrect };
    res.status(200).send(answer);
});

app.listen(3000, function () {
    console.log('MCQ app listening on port 3000!')
})

// code from https://www.frankmitchell.org/2015/01/fisher-yates/
function shuffle(array) {
    var i = 0
        , j = 0
        , temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}


function checkAnswer(id, choice) {
    return id % questions.length == choice;    //current logic is that the correct answer is the id;
    // for(i in questions) {
    //     if(questions[i].id == id) {
    //         return questions[i].choice == choice;
    //     }
    // }

    // return false;
}